# suckless-st

My personal clone of [st](https://st.suckless.org/) from suckless.

On Debian/Ubuntu you need `libx11-dev` and `libxft-dev` to compile `st`.
